
# CI / CD Pipeline :

CI/CD introduces ongoing automation and continuous monitoring throughout the lifecycle of apps, from integration and testing phases to delivery and [deployment]. Taken together, these connected practices are often referred to as a "[CI/CD pipeline] and are supported by development and operations teams working together in an agile way with either a DevOp sor [site reliability engineering (SRE)] approach.

CI/CD has a few different meanings. The "CI" in CI/CD always refers to continuous integration, which is an automation process for developers. Successful CI means new code changes to an app are regularly built, tested, and merged to a shared repository. It’s a solution to the problem of having too many branches of an app in development at once that might conflict with each other.

The "CD" in CI/CD refers to continuous delivery and/or continuous deployment, which are related concepts that sometimes get used interchangeably. Both are about automating further stages of the pipeline, but they’re sometimes used separately to illustrate just how much automation is happening.

Continuous delivery usually means a developer’s changes to an application are automatically bug tested and uploaded to a repository (like GitLab or a container registry), where they can then be deployed to a live production environment by the operations team. It’s an answer to the problem of poor visibility and communication between dev and business teams. To that end, the purpose of continuous delivery is to ensure that it takes minimal effort to deploy new code.

Continuous deployment (the other possible "CD") can refer to automatically releasing a developer’s changes from the repository to production, where it is usable by customers. It addresses the problem of overloading operations teams with manual processes that slow down app delivery. It builds on the benefits of continuous delivery by automating the next stage in the pipeline.

![CI/CD Flow](https://www.redhat.com/cms/managed-files/styles/wysiwyg_full_width/s3/ci-cd-flow-desktop.png?itok=2EX0MpQZ "CI/CD Flow")

 ## Pipeline  jobs and stages

Pipelines are the top-level component of continuous integration, delivery, and deployment.

Pipelines comprise:

- Jobs, which define what to do. For example, jobs that compile or test code.
- Stages, which define when to run the jobs. For example, stages that run tests after stages that compile the code.
- Jobs are executed by runners. Multiple jobs in the same stage are executed in parallel, if there are enough concurrent runners.

If all jobs in a stage succeed, the pipeline moves on to the next stage.

If any job in a stage fails, the next stage is not (usually) executed and the pipeline ends early. 

## Types of pipelines[](https://docs.gitlab.com/ee/ci/pipelines/#types-of-pipelines "Permalink")

![GitLab Ci/CD](docs/img/gitlab_workflow_example_11_9.png)

## CI -Merged results pipeline

![MRs](docs/img/MRs.png)

## CD - Deployment pipeline

![Deploy](docs/img/deploy.png)

### Deploy_Delta

Pipelines can be configured in many different ways:

-   [Basic pipelines](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#basic-pipelines)  run everything in each stage concurrently, followed by the next stage.
-   [Directed Acyclic Graph Pipeline (DAG) pipelines](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/index.html)  are based on relationships between jobs and can run more quickly than basic pipelines.
-   [Merge request pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html)  run for merge requests only (rather than for every commit).
-   [Merged results pipelines](https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html)  are merge request pipelines that act as though the changes from the source branch have already been merged into the target branch.
-   [Merge trains](https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html)  use merged results pipelines to queue merges one after the other.
-   [Parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html)  break down complex pipelines into one parent pipeline that can trigger multiple child sub-pipelines, which all run in the same project and with the same SHA. This pipeline architecture is commonly used for mono-repos.
-   [Multi-project pipelines](https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html)  combine pipelines for different projects together.


## Tekton

Tekton defines a number of [Kubernetes custom resources] as building blocks in order to standardize pipeline concepts and provide a terminology that is consistent across CI/CD solutions. These custom resources are an extension of the Kubernetes API that let users create and interact with these objects using `kubectl` and other Kubernetes tools.

![Tekton Architecture](docs/img/tekton_architecture.png)

The custom resources needed to define a pipeline are listed below:
* `Task`: a reusable, loosely coupled number of steps that perform a specific task (e.g. building a container image)
* `Pipeline`: the definition of the pipeline and the `Tasks` that it should perform
* `TaskRun`: the execution and result of running an instance of task
* `PipelineRun`: the execution and result of running an instance of pipeline, which includes a number of `TaskRuns`



## Fawry CI / CD Pipeline


```mermaid

sequenceDiagram
    autonumber
    par CI - Pipeline
        New MR->>Develop [ Delta / Omega ]: Merge into Develop
        Develop [ Delta / Omega ]->>Alpha: Merge into Alpha
       
    loop  Reg Testing 
        Alpha-->>New MR: MR Hot Fix from Alpha
        New MR-->>Alpha: Merge Hot Fix into Alpha
    end
    note over Alpha: Tag Alpha
        end 
    par CD - Pipeline
    Alpha->>Master:Deploy to Master
    note over Master: Release Beta 
    
    Master->>Staging: Deploy to Staging 
    Staging->>Production: Deploy to Production
    end
    
```

## CI - Merge request pipeline

![MR](docs/img/MR.png)


## Check_Updates

### Check Merge

```

Check_master:
  stage:  Check_Updates
  image: 
    name: docker.fawry.io/devops/ci:bash 
    entrypoint: ["/bin/bash", "-c"]
  script:
    - git rev-list HEAD > check_master.txt
    - remote_master_sha=$(git ls-remote https://CI-token:$CiTokenSecret@gitlab.fawry.io/$CI_PROJECT_PATH.git $CI_MERGE_REQUEST_TARGET_BRANCH_NAME | awk '{ print $1}')    
    - echo "$remote_master_sha"
    - cat check_master.txt    
    - echo "sha == $remote_master_sha"    
    - echo " Target branch ==  $CI_MERGE_REQUEST_TARGET_BRANCH_NAME  "    
    - |
       if grep -Fxq "$remote_master_sha"  ./check_master.txt
                then
                    echo "#########################################################################\n"
                    echo "         Your branch have latest updates "
                    echo "#########################################################################\n"
                    exit 0
                else
                    echo "###############################################################################################################################"
                    echo "    Please rebase latest updates from $CI_MERGE_REQUEST_TARGET_BRANCH_NAME   ---> $remote_master_sha  before push to your branch"
                    echo "###############################################################################################################################"
                    exit 1
                fi   
 
  allow_failure: true
  only:
        - merge_requests
  tags:
    - cidocker

```


### Check CHANGELOG

```
Check_Changelog:
  stage:  Check_Updates
  image: 
    name: docker.fawry.io/devops/ci:bash 
    entrypoint: ["/bin/bash", "-c"]
  script:
    - echo " Target branch ==  $CI_MERGE_REQUEST_TARGET_BRANCH_NAME  "    
    - current_changelog_commit=$(git rev-list HEAD CHANGELOG  | awk '{ print $1}'| awk 'NR==1{print $1}')
    - git checkout $CI_MERGE_REQUEST_TARGET_BRANCH_NAME 
    - git pull --all 
    - git status
    - git log --pretty=oneline | awk '{ print $1}' > /target_commits
    - echo "$current_changelog_commit"
    - |
       if grep -Fxq "$current_changelog_commit"  /target_commits
                then
                    echo "#########################################################################"
                    echo "          Please add log to CHANGELOG file                               "
                    echo "#########################################################################"
                    exit 1
                else
                    echo "#########################################################################"
                    echo "            Thanks for update CHANGELOG                                  "
                    echo "#########################################################################"
                    exit 0
                fi   
 
  allow_failure: true
  only:
        - merge_requests
  tags:
    - cidocker

```


### Dependencies -Maven_Dependencies.yml

```
Install_dependencies:
    image: docker.fawry.io/devops/ci:maven3.6.1-jdk-8
    stage: Maven_Dependencies
    script:
      -  mvn -DskipTests -s resources/ci/settings.xml  clean install
      - ls -lha /root/
      - ls -lha ./
      - cp -vr /root/.m2 ./
      - tar cvzf  m2.gz ./.m2
      - ls -lha
      - ls -l   ./*-rest/target/
      - cp -vr  ./*-rest/target/*.jar ./ 
    only:
      - develop
      - merge_requests
      - delta
      - omega
      - alpha
    tags:
      - cidocker   
    cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - .m2
        - ./*.jar
      policy: push

```

### Lint_Testing - Check_Style.yml

```
Check_style:
  stage:  Maven_Testing
 # image: docker.fawry.io/devops/ci:checkstyle
  image: docker.fawry.io/devops/ci:checkstyle-convert
  script:
    - cp -vr ./.m2 /root/
    - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
    - dockerd & sleep 10    
    -  mvn --batch-mode -V -U -e  -s resources/ci/settings.xml checkstyle:checkstyle
    -  ls -l ./*-rest/target/
    -  mkdir -p ./checkstyle/
    -  pwd 
    -  cp -vr ./*-rest/target/*.xml  ./checkstyle/
    -  docker run -v $PWD/checkstyle:/check --rm ci-report  convert -S checkstyle -I /check/checkstyle-result.xml -T junit -O /check/jcheckstyle-result.xml
    -  docker run -v $PWD/checkstyle:/check --rm ci-report convert -S checkstyle -I /check/checkstyle-result.xml -T gitlab-json -O /check/checkstyle-result.json
    - ls -l $PWD/checkstyle
  allow_failure: true
  only:
      - merge_requests
  tags:
    - cidocker    
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - .m2
        - ./*.jar
      policy: pull  
  artifacts:
    name: "CodeQuality-$CI_JOB_NAME"
    paths:
      -  ./checkstyle/*.xml
      -  ./checkstyle/*.json
    reports:
      junit: 
        -  ./checkstyle/jcheckstyle-result.xml
      codequality:
        - ./checkstyle/checkstyle-result.json


```


### Unit_Testing - Juint_Testing.yml

```
Junit_Testing:
  stage:  Maven_Testing
 # image: docker.fawry.io/devops/ci:checkstyle
  image: docker.fawry.io/devops/ci:checkstyle-convert
  script:
    - cp -vr ./.m2 /root/
    - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
    - dockerd & sleep 10    
    -  mvn --batch-mode -V -U -e  -s resources/ci/settings.xml test
    -  ls -l ./*-rest/target/
    -  mkdir -p ./unittest/
    -  pwd 
    -  cp -vr ./*-rest/target/surefire-reports/TEST-*.xml  ./unittest/
#    -  docker run -v $PWD/unittest:/check --rm ci-report  convert -S checkstyle -I /check/unittest-result.xml -T junit -O /check/jcheckstyle-result.xml
    -  docker run -v $PWD/unittest:/check --rm ci-report convert -S checkstyle -I /check/TEST-*.xml -T gitlab-json -O /check/unittest-result.json
    - ls -l $PWD/unittest
  allow_failure: true
  only:
      - merge_requests
  tags:
    - cidocker    
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - .m2
        - ./*.jar
      policy: pull  
  artifacts:
    name: "Unit_Testing-$CI_JOB_NAME"
    paths:
      -  ./unittest/*.xml
      -  ./unittest/*.json
    reports:
      junit:
        - ./*-rest/target/surefire-reports/TEST-*.xml
      codequality:
        - ./unittest/unittest-result.json


```


### Integration_Testing - maven_verify.yml

```
API_Postman_Testing:
  image: docker.fawry.io/devops/ci:api-newman
  stage:  360_Degree_Testing
  script:
    - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
    - sleep 10s
    - dockerd & sleep 10 
    - docker run postman/newman_alpine33 run "https://www.getpostman.com/collections/0d0350a9a89d39fb6361"  --reporters cli,html,junit --reporter-html-export postmanreport.html --reporter-junit-export apipostman.xml 
    -  mkdir -p ./newmanjson/
    -  pwd 
    -  cp -vr ./*.xml  ./newmanjson/
    -  docker run -v $PWD/newmanjson:/check --rm docker.fawry.io/devops/ci:report-ci-convert convert -S checkstyle -I /check/apipostman.xml -T gitlab-json -O /check/newman-result.json
  tags:
    - cidocker 
  only:
    - merge_requests
  artifacts:
    name: "API_Testing_Postman-$CI_JOB_NAME"
    paths:
      - ./postmanreport.html
    reports:
      junit:
        - ./apipostman.xml
      codequality:
        - ./newmanjson/newman-result.json
 
```


## CI -Merged results pipeline

![MRs](docs/img/MRs.png)

### Deploy_Delta

```
Deploy_Delta:
  image: docker.fawry.io/devops/ci:deploy-maven-170
  stage: Delta
  script:
     - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
     - dockerd & sleep 10
     - sh /createdockerfile.sh
     - docker build . -t docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - docker push docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - sh /k8s-yml-script.sh
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - kubectl apply -f $CI_COMMIT_REF_NAME-deploy.yml
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - sleep 10s
     - kubectl get pods -A | grep  $CI_PROJECT_NAME
     - |
        echo "##############################################################################################################"
        echo "       $CI_PROJECT_NAME $CI_COMMIT_REF_NAME env --->  http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/ "
        echo "##############################################################################################################"
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - ./*.jar
      policy: pull
  tags:
    - cidocker   
  environment:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME 
    url: http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/
  only:
    - delta


```


### Deploy_Omega

```

Deploy_Omega:
  image: docker.fawry.io/devops/ci:deploy-maven-170
  stage: Omega
  script:
     - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
     - dockerd & sleep 10
     - sh /createdockerfile.sh
     - docker build . -t docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - docker push docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - sh /k8s-yml-script.sh
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - kubectl apply -f $CI_COMMIT_REF_NAME-deploy.yml
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - sleep 10s
     - kubectl get pods -A | grep  $CI_PROJECT_NAME
     - |
        echo "##############################################################################################################"
        echo "       $CI_PROJECT_NAME $CI_COMMIT_REF_NAME env --->  http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/ "
        echo "##############################################################################################################"
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - ./*.jar
      policy: pull
  tags:
    - cidocker   
  environment:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME 
    url: http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/
  only:
    - omega


```


## CD - Deployment pipeline

![Deploy](docs/img/deploy.png)


### Merge_to_alpha - Merge_Delta_into_Alpha.yml

```


Merge_Delta_into_Alpha:
  stage:  Merge_Into_Alpha
  image: 
    name: docker.fawry.io/devops/ci:bash 
    entrypoint: ["/bin/bash", "-c"]
  script:
    - git checkout delta  
    - git rev-list HEAD > check_delta.txt
    - remote_alpha_sha=$(git ls-remote https://CI-token:$CiTokenSecret@gitlab.fawry.io/$CI_PROJECT_PATH.git alpha | awk '{ print $1}')    
    - cat check_delta.txt    
    - echo "sha == $remote_alpha_sha"    
    - |
       if grep -Fxq "$remote_alpha_sha"  ./check_delta.txt
                then
                    echo "#########################################################################\n"
                    echo "         Delta branch have latest updates of Alpha "
                    echo "#########################################################################\n"
                else
                    echo "###############################################################################################################################"
                    echo "    Please rebase latest updates from Alpha   ---> Delta before Merge"
                    echo "###############################################################################################################################"
                    exit 1
                fi   
    -  mkdir -p /merge 
    -  cd /merge 
    -  git clone https://CI-token:$CiTokenSecret@gitlab.fawry.io/$CI_PROJECT_PATH.git
    -  cd $CI_PROJECT_NAME
    -  git pull --all
    -  git checkout alpha
    -  git rebase delta
    -  git push           
  when: manual
  only:
        - alpha
  tags:
    - cidocker


```

### Deploy_Alpha

```
Deploy_Alpha:
  image: docker.fawry.io/devops/ci:deploy-maven-170
  stage: Alpha
  script:
     - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
     - dockerd & sleep 10
     - sh /createdockerfile.sh
     - docker build . -t docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - docker push docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - sh /k8s-yml-script.sh
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - kubectl apply -f $CI_COMMIT_REF_NAME-deploy.yml
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - sleep 10s
     - kubectl get pods -A | grep  $CI_PROJECT_NAME
     - |
        echo "##############################################################################################################"
        echo "       $CI_PROJECT_NAME $CI_COMMIT_REF_NAME env --->  http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/ "
        echo "##############################################################################################################"
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - ./*.jar
      policy: pull
  tags:
    - cidocker   
  environment:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME 
    url: http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/
  only:
    - alpha


```



### Tag Alpha

```
Tag_Alpha:
  stage: Alpha
  image: docker.fawry.io/devops/ci:maven3.6.1-jdk-8
  script:
    -  ls -lha
    -  cp -vr ./.m2 /root/
    -  mkdir -p /tagging
    -  cd /tagging
    -  export GIT_SSL_NO_VERIFY=1
    -  git clone https://CI-token:$CiTokenSecret@gitlab.fawry.io/$CI_PROJECT_PATH.git --branch alpha
    -  cd ./$CI_PROJECT_NAME
    -  git config user.name CI-Token
    -  git config user.email CI-Token@fawry.com
    -  mvn --batch-mode  -s resources/ci/settings.xml -DskipTests  release:clean release:prepare  release:perform
    -  mvn help:evaluate -Dexpression=project.version -q -DforceStdout
  only:
    - alpha
  when: manual   
  tags:
    - cidocker  
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - .m2
        - ./*.jar
      policy: pull


```


### Deploy_Beta - deploy_beta.yml

```



### Deploy Beta

Deploy_Beta [ UAT ]:
  image: docker.fawry.io/devops/ci:deploy-maven-170
  stage: Beta [ UAT ]
  script:
     - rm -rf /var/lib/docker/runtimes && rm -rf /var/lib/docker/tmp
     - dockerd & sleep 10
     - sh /createdockerfile.sh
     - docker build . -t docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - docker push docker.fawry.io/$CI_PROJECT_PATH/$CI_COMMIT_REF_NAME:$CI_COMMIT_SHORT_SHA
     - sh /k8s-yml-script.sh
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - kubectl apply -f $CI_COMMIT_REF_NAME-deploy.yml
     - cat $CI_COMMIT_REF_NAME-deploy.yml
     - sleep 10s
     - kubectl get pods -A | grep  $CI_PROJECT_NAME
     - |
        echo "##############################################################################################################"
        echo "       $CI_PROJECT_NAME $CI_COMMIT_REF_NAME env --->  http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/ "
        echo "##############################################################################################################"
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - ./*.jar
      policy: pull
  tags:
    - cidocker   
  environment:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_NAME 
    url: http://$CI_COMMIT_REF_NAME.$CI_PROJECT_ROOT_NAMESPACE.fawry.io/$CI_PROJECT_NAME/
  when: manual   
  only:
    - alpha

```

###  Releasing UAT

```

Release_UAT:
  stage: Beta [ UAT ]
  image: docker.fawry.io/devops/ci:checkstyle-convert
  script:
    -  mvn help:evaluate -Dexpression=project.version -q -DforceStdout
    -  cp -vr ./.m2 /root/
    -  mkdir -p /tagging
    -  cd /tagging
    -  export GIT_SSL_NO_VERIFY=1
    -  git clone https://CI-token:$CiTokenSecret@gitlab.fawry.io/$CI_PROJECT_PATH.git --branch alpha
    -  cd ./$CI_PROJECT_NAME
    -  git config user.name gitlab.ci
    -  git config user.email gitlab-bot.ci@fawry.com
    -  mvn  -s resources/ci/settings.xml -DskipTests=true  release:clean release:prepare  release:perform
  release:
    name: 'Release $TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'  # $EXTRA_DESCRIPTION and the $TAG
    tag_name: '$TAG'                                                 # variables must be defined elsewhere
    ref: '$CI_COMMIT_SHA'                                            # in the pipeline. For example, in the 
    released_at: '2020-07-15T08:00:00Z'  # Optional, is auto generated if not defined, or can use a variable.
    assets:
      links:
        - name: 'Release Docker:'
          url: 'docker'
        - name: 'asset2'
          url: 'https://example.com/assets/2'
          filepath: '/pretty/url/1' # optional
          link_type: 'other' # optional
  only:
    - alpha
  when: manual   
  tags:
    - cidocker  
  cache:
      key: $CI_COMMIT_SHORT_SHA
      paths:
        - .m2
      policy: pull


```
